.\"	$OpenBSD: acme-client.1,v 1.7 2016/09/01 13:42:45 jmc Exp $
.\"
.\" Copyright (c) 2016 Kristaps Dzonsons <kristaps@bsd.lv>
.\"
.\" Permission to use, copy, modify, and distribute this software for any
.\" purpose with or without fee is hereby granted, provided that the above
.\" copyright notice and this permission notice appear in all copies.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
.\" WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
.\" MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
.\" ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
.\" WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
.\" ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
.\" OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
.\"
.Dd $Mdocdate: September 1 2016 $
.Dt ACME-CLIENT 1
.Os
.Sh NAME
.Nm acme-client
.Nd ACME client
.Sh SYNOPSIS
.Nm acme-client
.Op Fl bFmNnrv
.Op Fl a Ar agreement
.Op Fl C Ar challengedir
.Op Fl c Ar certdir
.Op Fl f Ar accountkey
.Op Fl k Ar domainkey
.Op Fl s Ar authority
.Ar domain
.Op Ar altnames
.Sh DESCRIPTION
The
.Nm
utility is an
Automatic Certificate Management Environment (ACME) client.
It submits an X509 certificate for
.Ar domain
and its alternate DNS names
.Ar altnames
to an ACME authority server for automated signing,
and it can also revoke previously submitted signatures.
It must be run as root
(see
.Xr chroot 2 ) .
.Pp
By default, it uses
.Pa /var/www/acme
for responding to challenges
.Pq Fl C ,
.Pa /etc/ssl/acme
for the public certificate directory
.Pq Fl c ,
.Pa /etc/ssl/acme/private/privkey.pem
for the domain private key
.Pq Fl k ,
and
.Pa /etc/acme/privkey.pem
for the account private key
.Pq Fl f .
All of these must exist unless
.Fl n
and/or
.Fl N
are being used,
which generates an account and domain private keys, respectively.
.Pp
The options are as follows:
.Bl -tag -width Ds
.It Fl a Ar agreement
Use an alternative agreement URL.
The default uses the current one, but it may be out of date.
.It Fl b
Back up all certificates in the certificate directory.
This only happens if a remove or replace operation is possible.
The backups are named
.Pa cert-NNNNN.pem ,
.Pa chain-NNNNN.pem ,
and
.Pa fullchain-NNNNN.pem ,
where
.Li NNNNN
is the current
.Ux
Epoch.
Any given backup uses the same Epoch time for all three certificates.
If there are no certificates in place, this option does nothing.
.It Fl C Ar challengedir
The directory to register challenges.
See
.Sx Challenges
for details.
.It Fl c Ar certdir
The directory to store public certificates.
See
.Sx Certificates
for details.
.It Fl F
Force updating the certificate signature even if it's too soon.
.It Fl f Ar accountkey
The account private key.
This was either made with a previous client or with
.Fl n .
.It Fl k Ar domainkey
The private key for the domain.
This may also be created with
.Fl N .
.It Fl m
Append
.Ar domain
to all default paths except the challenge path
.Pq i.e. those that are overridden by Fl c , k , f .
Thus,
.Ar foo.com
as the initial domain would make the default domain private key into
.Pa /etc/ssl/acme/private/foo.com/privkey.pem .
This is useful in setups with multiple domain sets.
.It Fl N
Create a new 4096-bit RSA domain key if one does not already exist.
.It Fl n
Create a new 4096-bit RSA account key if one does not already exist.
.It Fl r
Revoke the X509 certificate found in the certificates.
.It Fl s Ar authority
ACME
.Ar authority
to talk to.
Currently the following authorities are available:
.Pp
.Bl -tag -width "letsencrypt-staging" -compact
.It Cm letsencrypt
Let's Encrypt authority
.It Cm letsencrypt-staging
Let's Encrypt staging authority
.El
.Pp
The default is
.Cm letsencrypt .
.It Fl v
Verbose operation.
Specify twice to also trace communication and data transfers.
.It Ar domain
The domain name.
The only difference between this and
.Ar altnames
is that it's put into the certificate's
.Li CN
field and it uses the
.Qq main
domain when specifying
.Fl m .
.It Ar altnames
Alternative names
.Pq Dq SAN
for the domain name.
The number of SAN entries is limited to 100 or so.
.El
.Pp
The process by which
.Nm
obtains signed certificates is roughly as follows.
.Bl -enum
.It
Access the CA (unauthenticated) and request its list of resources.
.It
Optionally create and register a new RSA account key.
.It
Read and process the RSA account key.
This is used to authenticate each subsequent communication to the CA.
.It
For each domain name:
.Pp
.Bl -enum -compact
.It
submit a challenge for authentication to the CA
.It
create a challenge response file
.It
wait until the CA has verified the challenge
.El
.It
Read and extract the domain key.
.It
Create an X509 request from the doman key for the domain and its
alternative names.
.It
Submit a request for signature to the CA.
.It
Download the signed X509 certificate.
.It
Extract the CA issuer from the X509 certificate.
.It
Download the certificate chain from the issuer.
.El
.Pp
The revocation sequence is similar:
.Bl -enum
.It
Request a list of resources, and manage the RSA account key as in the case for
signing.
.It
Read and extract the X509 certificate (if found).
.It
Create an X509 revocation request.
.It
Submit a request for revocation to the CA.
.It
Remove the certificate, the chain, and the full-chain.
.El
.Ss Challenges
Challenges are used to verify that the submitter has access to
the registered domains.
.Nm
implements only the
.Dq http-01
challenge type, where a file is created within a directory accessible by
a locally-run web server configured for the requested domain.
The default challenge directory
.Pa /var/www/acme
can be served by
.Xr httpd 8
with this location block:
.Bd -literal
	location "/.well-known/acme-challenge/*" {
		root "/acme"
		root strip 2
	}
.Ed
.Pp
This way, the files placed in
.Pa /var/www/acme
will be properly mapped by the web server during response challenges
with the authority server.
.Ss Certificates
Public certificates (domain certificate, chain, and the full-chain) are
placed by default in
.Pa /etc/ssl/acme
as
.Pa cert.pem ,
.Pa chain.pem ,
and
.Pa fullchain.pem ,
respectively.
These are all created as the root user with mode 444.
.Pp
The
.Pa cert.pem
file, if found, is checked for its expiration: if more than 30 days from
expiry,
.Nm
will not attempt to refresh the signature.
.Sh EXIT STATUS
.Nm
returns 1 on failure, 2 if the certificates didn't change (up to date),
or 0 if certificates were changed (revoked or updated).
.Sh EXAMPLES
To create and submit a new key for a single domain, assuming that the
web server has already been configured to map the challenge directory
as in the
.Sx Challenges
section:
.Bd -literal
# acme-client -vNn foo.com www.foo.com smtp.foo.com
.Ed
.Pp
A daily
.Xr cron 8
job can renew the certificates:
.Bd -literal
#! /bin/sh

acme-client foo.com www.foo.com smtp.foo.com

if [ $? -eq 0 ]
then
	/etc/rc.d/httpd reload
fi
.Ed
.Sh SEE ALSO
.Xr openssl 1 ,
.Xr httpd.conf 5
.Sh STANDARDS
.Rs
.%U https://tools.ietf.org/html/draft-ietf-acme-acme-03
.%T Automatic Certificate Management Environment (ACME)
.Re
.Sh AUTHORS
The
.Nm
utility was written by
.An Kristaps Dzonsons Aq Mt kristaps@bsd.lv .
.Sh BUGS
The challenge and certificate processes currently retain their (root)
privileges.
.Pp
For the time being,
.Nm
only supports RSA as an account key format.
